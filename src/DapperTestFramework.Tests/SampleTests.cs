using FluentAssertions;
using Xunit;

namespace DapperTestFramework.Tests
{
    public class SampleTests
    {
        private readonly Sample sut;
        
        public SampleTests()
        {
            sut = new Sample();
        }

        [Fact]
        public void WhenTested_ItShouldReturnTrue()
        {
            var value = sut.IsTrue();

            value
                .Should().BeTrue();
        }
    }
}
