[![Build status](https://ci.appveyor.com/api/projects/status/06rugy06pa2v5w83/branch/master?svg=true)](https://ci.appveyor.com/project/joekucera2002/dappertestframework/branch/master)
[![codecov](https://codecov.io/bb/joekucera2002/dappertestframework/branch/master/graph/badge.svg)](https://codecov.io/bb/joekucera2002/dappertestframework)

# DapperTestFramework

A framework for database integration testing using Dapper.