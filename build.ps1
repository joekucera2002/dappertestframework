properties {
    $base_dir = $PSScriptRoot
    $source_dir = "$base_dir\src"
    $project_name = "DapperTestFramework"
    $solution_dir = "$source_dir\$project_name"
    $solution_file = "$source_dir\$project_name.sln"
    $tests_dir = "$source_dir\tests"
    $packages_dir = "$env:USERPROFILE\.nuget\packages"
    $open_cover = "$packages_dir\OpenCover\4.6.519\tools\OpenCover.Console.exe"
    $code_cov = "$packages_dir\codecov\1.0.5\tools\codecov.exe"
}

task default -depends Clean, Compile, TestCover, Package 

task Clean {
    Write-Host "`r"

    exec { dotnet clean $solution_file -c Release }

    Write-Host "Cleaned Release"
    Write-Host "`r"
}

task Compile -depends Clean {
    Write-Host "`r"
    Write-Host "Branch: `t$env:APPVEYOR_REPO_BRANCH"
    Write-Host "Revision: `t$env:APPVEYOR_BUILD_NUMBER"

    if ($env:APPVEYOR_REPO_TAG -eq "true") { 
        Write-Host "Tag: `t`t$env:APPVEYOR_REPO_TAG_NAME"
    }

    Write-Host "`r"

    exec { dotnet --info }
    exec { dotnet build $solution_file -c Release --version-suffix $env:APPVEYOR_BUILD_NUMBER }

    Write-Host "`r"
}

task Package -depends Compile {
    Write-Host "`r"

    exec { dotnet pack $solution_file -c Release --no-build --no-restore --include-symbols }

    Write-Host "`r"
}

task TestCover -depends Compile {
    Write-Host "`r"

    $files = Get-ChildItem -Recurse -File *.Tests.dll | ?{$_.FullName -notmatch "\\obj\\|\\Debug\\"}

    & $open_cover -register:user -target:dotnet.exe -targetargs:"vstest $files" -oldStyle -filter:"+[$project_name*]*" -output:coverage.xml

    Write-Host $code_cov

    & $code_cov -f "coverage.xml" -t $env:CODECOV_REPO_TOKEN

    Write-Host "`r"
}
